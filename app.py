from flask import Flask, request, jsonify
from flask import render_template
import sqlite3

app = Flask(__name__, template_folder='templates', static_folder='static')

#Test commit
@app.route('/')
def home():
    return render_template('index.html')


@app.route('/tasks', methods=['GET'])
def get_tasks():
    connection = sqlite3.connect('database.db')
    cursor = connection.cursor()

    cursor.execute('SELECT * FROM tasks')
    tasks = cursor.fetchall()

    return jsonify(tasks)


@app.route('/tasks', methods=['POST'])
def add_task():
    new_task = request.json['content']

    connection = sqlite3.connect('database.db')
    cursor = connection.cursor()

    cursor.execute(f'INSERT INTO tasks (content) VALUES ("{new_task}")')
    connection.commit()

    return {'id': cursor.lastrowid, 'content': new_task}, 201


@app.route('/tasks/<int:task_id>', methods=['PUT'])
def update_task(task_id):
    updated_task = request.json['content']

    connection = sqlite3.connect('database.db')
    cursor = connection.cursor()

    cursor.execute(f'UPDATE tasks SET content = "{updated_task}" WHERE id = {task_id}')
    connection.commit()

    return '', 204


@app.route('/tasks/<int:task_id>', methods=['DELETE'])
def delete_task(task_id):
    connection = sqlite3.connect('database.db')
    cursor = connection.cursor()

    cursor.execute(f'DELETE FROM tasks WHERE id = {task_id}')
    connection.commit()

    return '', 204


if __name__ == "__main__":
    app.run(debug=True)
