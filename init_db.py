import sqlite3


if __name__ == "__main__":
    connection = sqlite3.connect('database.db')
    cursor = connection.cursor()

    cursor.execute('''
        CREATE TABLE tasks(
            id INTEGER PRIMARY KEY,
            content TEXT NOT NULL
        );
    ''')

    connection.commit()
    connection.close()
