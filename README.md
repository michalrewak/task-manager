# Prosty menedżer zadań (Task Manager)
Do wykonania jest projekt z dwoma elementami:
- Baza danych postawiona (co najmniej) lokalnie.
- API łączące się z bazą i pozwalające na komunikację z bazą danych.
- Nieskomplikowany frontend.

Ten projekt to prosty menedżer zadań, który umożliwia dodawanie, wyświetlanie, aktualizowanie i usuwanie zadań. Jest zbudowany przy użyciu Flask (Python) dla backendu, SQLite dla bazy danych, a jQuery i HTML dla frontendu.

## Funkcje

1. Dodawanie nowych zadań
2. Wyświetlanie listy wszystkich zadań
3. Aktualizowanie treści istniejących zadań
4. Usuwanie zadań

## Jak uruchomić

Aby uruchomić ten projekt lokalnie, wykonaj następujące kroki:

1. Sklonuj repozytorium: `git clone https://gitlab.com/michalrewak/task-manager.git`
2. Przejdź do katalogu projektu: `cd task-manager`
3. Stwórz bazę danych SQLite: `python init_db.py`
4. Uruchom aplikację Flask: `python app.py`
5. Otwórz przeglądarkę i przejdź do `http://localhost:5000`

## Zależności

- Python 3.8+
- Flask
- SQLite
- jQuery

## Autor

Michał Rewak

## Licencja

Ten projekt jest udostępniany na licencji MIT.
